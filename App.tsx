import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
//import { StackNavigator } from './src/navigator/StackNavigator';
//import { MenuLateralBasico } from './src/navigator/MenuLateralBasico';
import 'react-native-gesture-handler';
import { MenuLateral } from './src/navigator/MenuLateral';
import { Tabs } from './src/navigator/Tabs';
import { Authprovider } from './src/context/AuthContext';
export default function App() {
  return (
    <NavigationContainer>
      <AppState>
        {/* <MenuLateralBasico /> */}
        <MenuLateral />
        {/* <Tabs /> */}
      </AppState>
    </NavigationContainer>
  );
}

const AppState = ( { children }: any ) => {
  return (
    <Authprovider>
      { children }
    </Authprovider>
  )
}
