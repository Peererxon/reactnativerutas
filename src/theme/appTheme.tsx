import { StyleSheet } from "react-native";
/* Centralizando los colores */
export const colors = {
    primary : '#5856D6'
}

export const styles = StyleSheet.create({
    globalMargin:{
        marginHorizontal: 20
    },
    title: {
        fontSize: 20,
        marginBottom: 10
    },
    botonGrande: {
        width: 100,
        height: 100,
        backgroundColor: 'red',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        marginTop: 10
    },
    botonGrandeTexto: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    },
    avatarContainer:{
        alignItems: 'center',
        marginTop: 20
    },
    menuContainer:{
        marginVertical: 30,
        alignItems: 'center'
    },
    menuBoton:{
        marginVertical: 8
    },
    menuTexto:{
        fontSize: 22,
        fontWeight: 'bold'
    },
    avatar: {
        height: 150,
        width: 150,
        borderRadius: 100
    }
})