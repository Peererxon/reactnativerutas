import React from 'react';
import { createDrawerNavigator, DrawerContentComponentProps, DrawerContentScrollView } from '@react-navigation/drawer';
import { SettingsScreen } from '../screen/SettingsScreen';
import { StackNavigator } from './StackNavigator';
import { Image, Text, TouchableOpacity, useWindowDimensions, View } from 'react-native';
import { colors, styles } from '../theme/appTheme';
import { Tabs } from './Tabs';
import Icon from 'react-native-vector-icons/Ionicons';

const Drawer = createDrawerNavigator();

export const MenuLateral = () => {

  const { width } = useWindowDimensions();

  return (
    <Drawer.Navigator
      screenOptions={
        {
          drawerType: width >= 768 ? 'permanent' : 'front',
          drawerPosition: 'right',
          headerShown: false,
        }
      }
      drawerContent={ props=> <MenuInterno { ...props } /> }

    >
      {/* <Drawer.Screen name="StackNavigator" options={{ title: 'navegacion 1' }} component={StackNavigator} /> */}
      <Drawer.Screen name="Tabs" component={ Tabs }/>
      <Drawer.Screen name="SettingsScreen" component={ SettingsScreen } />
    </Drawer.Navigator>
  );
}

export const MenuInterno= ( { navigation }: DrawerContentComponentProps<DrawerContentOptions>  ) => {
  return ( 
    <DrawerContentScrollView>
      <View style={ styles.avatarContainer }>
        <Image source={{
            uri: 'https://electronicssoftware.net/wp-content/uploads/user.png'
          }}
          style={ styles.avatar }
        />
      </View>

      {/* Opciones de menu */}

      <View style={ {...styles.menuContainer, alignItems: 'flex-start'  }}>
          <TouchableOpacity  
            style={{
              ...styles.menuBoton,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-around',
              width: '80%',
              marginLeft: 10
            }}
            onPress={ ()=> navigation.navigate('Tabs') }
          >
            <Icon  name='map' color={ colors.primary } size={ 25 } /> 
            <Text  style={ styles.menuTexto }>Navegacion</Text>
          </TouchableOpacity>

          <TouchableOpacity  
            style={{
              ...styles.menuBoton,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-around',
              width: '80%',
            }}
            onPress={ ()=> navigation.navigate('SettingsScreen') }
          >
            <Icon  name='settings' color={ colors.primary } size={ 25 } />
            <Text  style={ styles.menuTexto }>Settings</Text>
          </TouchableOpacity>
      </View>
    </DrawerContentScrollView>
  );
}
