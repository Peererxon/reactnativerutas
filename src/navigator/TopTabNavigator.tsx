import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import React from 'react'
import { LogBox, Text } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import  Icon from 'react-native-vector-icons/Ionicons';
import AlbumScreen from '../screen/AlbumScreen';
import ChatScreen from '../screen/ChatScreen';
import ContactScreen from '../screen/ContactScreen';
import { colors } from '../theme/appTheme';
const Tab = createMaterialTopTabNavigator();
//para ignorar los warning 8no recomendado)
//LogBox.ignoreLogs('Sending')
export const TopTabNavigator = () => {

  const  { top:paddingTop } = useSafeAreaInsets()
  return (
    <Tab.Navigator
      style={{
        paddingTop,
      }}

      sceneContainerStyle={{
        backgroundColor: 'white'
      }}

      screenOptions={{
        tabBarIndicatorStyle: {
          backgroundColor: colors.primary,
        },
        tabBarPressColor: colors.primary,
        tabBarStyle: {
          elevation: 0,
          borderTopWidth: 0,
        },
        tabBarShowIcon: true  
      }}

    >
      {/* Se pueden agreagar los nombres sin ser iguales al componente cuando se asume que no tendra redirecciones con el navigator (no usaras la ruta en otro lugar) */}
      <Tab.Screen name="Album" component={AlbumScreen} options={{ tabBarIcon: ( { color } )=> <Text style={{ color  }}> <Icon  name='albums' size={ 20 } /></Text> }}/>
      <Tab.Screen name="Chat" component={ ChatScreen } options={{ tabBarIcon: ( { color } )=> <Text style={{ color  }}> <Icon name='chatbox-ellipses-outline' size={ 20 } /> </Text> }}/>
      <Tab.Screen name="Contact" component={ ContactScreen } options={{ tabBarIcon: ( { color } )=> <Text style={{ color  }}> <Icon name='call-outline' size={ 20 } /> </Text> }} />
    </Tab.Navigator>
  );
}