import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { SettingsScreen } from '../screen/SettingsScreen';
import { StackNavigator } from './StackNavigator';
import { useWindowDimensions } from 'react-native';

const Drawer = createDrawerNavigator();

export const MenuLateralBasico = () => {

  const { width } = useWindowDimensions();

  return (
    <Drawer.Navigator
      screenOptions={
        {
          drawerType: width >= 768 ? 'permanent' : 'front',
          drawerPosition: 'right',
          headerShown: false
        }
        
      }

    >
      <Drawer.Screen name="StackNavigator" options={{ title: 'navegacion 1' }} component={StackNavigator} />
      <Drawer.Screen name="Settings" component={ SettingsScreen } />
    </Drawer.Navigator>
  );
}