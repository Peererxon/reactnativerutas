
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Page1Screen from '../screen/Page1Screen';
import Page2Screen from '../screen/Page2Screen';
import Page3Screen from '../screen/Page3Screen';
import PersonScreen from '../screen/PersonScreen';

const Stack = createStackNavigator<RootStackParams>();

export type RootStackParams = {
  Page1Screen: undefined
  Page2Screen: undefined
  Page3Screen: undefined,
  PersonScreen: { id: number, nombre: string }
}

export const StackNavigator = () => {
  return (
    <Stack.Navigator
    //initialRouteName='Page2Screen'
    screenOptions={{
      //elevation eimina la linea de android
        headerStyle: {
            elevation: 0,
            shadowColor: 'transparent'
        },
        cardStyle: {
            backgroundColor: 'white'
        }
    }}
    >
      <Stack.Screen name="Page1Screen" options={{ title: 'Página1' }} component={ Page1Screen } />
      <Stack.Screen name="Page2Screen" options={{ title: 'Página2' }} component={ Page2Screen } />
      <Stack.Screen name="Page3Screen" options={{ title: 'Página3' }} component={ Page3Screen } />
      <Stack.Screen name="PersonScreen" component={ PersonScreen } />
    </Stack.Navigator>
  );
}