import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { Tab1Screen } from '../screen/Tab1Screen';
import { Tab2Screen } from '../screen/Tab2Screen';
//import { Tab3Screen } from '../screen/Tab3Screen';
import { StackNavigator } from './StackNavigator';
import { colors } from '../theme/appTheme';
import { Platform, Text } from 'react-native';
import { TopTabNavigator } from './TopTabNavigator';
import Icon from 'react-native-vector-icons/Ionicons';


export const Tabs = ()=> {
  return Platform.OS === 'ios' 
  ? <TabsIOS />
  : <TabsAndroid />
}



const BottomTabAndroid = createMaterialBottomTabNavigator();

const TabsAndroid = () => {
  return (
    <BottomTabAndroid.Navigator
      sceneAnimationEnabled={ true }
      barStyle={{
        backgroundColor : colors.primary,
        elevation: 0,
        borderTopWidth: 0,
      }}
      
      screenOptions= {
        
        ( {route} ) => ({
          
          tabBarIcon: ( { color,focused } ) =>{
            
            let iconName = '';

            switch( route.name ){
              case 'Tab1Screen' :
                iconName = 'T1'
                break;

              case 'Tab2Screen' :
                iconName = 'T2'
                break;
              
              case 'StackNavigator' :
                iconName = 'St'
                break;  
              
              case 'TopTabNavigator':
                iconName = 'Tp'
                break;
            }

            return <Text style={{ color }}  > { iconName } </Text>
          }
        })
      }
    >
      <BottomTabAndroid.Screen name="Tab1Screen" options={{ title: 'tab1', tabBarIcon: ( { color } )=> <Text style={{ color  }}> T1 </Text> }} component={ Tab1Screen } />
      <BottomTabAndroid.Screen name="Tab2Screen" options={{ title: 'tab2' }} component={ Tab2Screen } />
      <BottomTabAndroid.Screen name="StackNavigator" options={{ title: 'stack', tabBarIcon: ( { color } )=> <Text style={{ color  }}> <Icon  name='people-outline' size={ 20 } /> </Text> }} component={ StackNavigator } />
      <BottomTabAndroid.Screen name="TopTabNavigator" options={{ title: 'stack' }} component={ TopTabNavigator } />
    </BottomTabAndroid.Navigator>
  );
}


const BottomTabIos = createBottomTabNavigator();

const TabsIOS = () => {
  return (
    <BottomTabIos.Navigator
      sceneContainerStyle={{
        backgroundColor: 'white'
      }}
      /* Con el border eliminamos la linea gris */
      // FIXME: como puedo agregar estilos a la vez que la vaina de las rutas???
/*       screenOptions={{
        tabBarActiveTintColor : colors.primary,
        tabBarStyle : {
          elevation: 0,
          borderTopWidth: 0
        },
        tabBarLabelStyle:{
          fontSize: 15
        },
        tabBarIcon: ( { colors,focused, size } ) =>{
          return <Text>T1</Text>
        }
      }} */
      screenOptions= {
        
        ( {route} ) => ({
          
          tabBarIcon: ( { color,focused, size } ) =>{
            
            let iconName = '';

            switch( route.name ){
              case 'Tab1Screen' :
                iconName = 'T1'
                break;

              case 'Tab2Screen' :
                iconName = 'T2'
                break;
              
              case 'StackNavigator' :
                iconName = 'St'
                break;
              
              case 'TopTabNavigator':
                iconName = 'Tab'
                break;
            }

            return <Text style={{ color }}  > { iconName } </Text>
          }
        })
      }
      >
      {/* <Tab.Screen name="Tab1Screen" options={{ title: 'tab1', tabBarIcon: ( props )=> <Text style={{ color: props.color  }}>T1</Text> }} component={ Tab1Screen } /> */}
      <BottomTabIos.Screen name="Tab1Screen" options={{ title: 'tab1', tabBarIcon: ( props )=> <Text style={{ color: props.color  }}>T1</Text> }} component={ Tab1Screen } />
      <BottomTabIos.Screen name="Tab2Screen" options={{ title: 'tab2' }} component={ Tab2Screen } />
      <BottomTabIos.Screen name="StackNavigator" options={{ title: 'stack' }} component={ StackNavigator } />
      <BottomTabIos.Screen name="TopTabNavigator" options={{ title: 'toptab' }} component={ TopTabNavigator } />
    </BottomTabIos.Navigator>
  );
}