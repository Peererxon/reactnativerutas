import { View, Text, Button } from 'react-native';
import React, { useEffect } from 'react';
import { useNavigation } from '@react-navigation/core';
import { styles } from '../theme/appTheme';

export default function Page2Screen() {

  const navigator = useNavigation()

  useEffect(() => {
    navigator.setOptions({
      headerTitle: 'Atras'
    })
  
  }, []);
  
  return (
    <View style={ styles.globalMargin }>
      <Text style={ styles.title }>Page 2 Screen </Text>

      <Button
        title="ir a pagina 3"
        onPress={ ()=> navigator.navigate("Page3Screen") }
      />
    </View>
  );
}
