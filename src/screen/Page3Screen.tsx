import { View, Text, Button } from 'react-native';
import React from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { styles } from '../theme/appTheme';

interface Props extends StackScreenProps<any,any>{}

export default function Page3Screen( { navigation }: Props ) {
  return (
    <View style={ styles.globalMargin } >
      <Text style={ styles.title }>Page 3 Screen </Text>

      <Button 
        title='Regresar'
        onPress={ ()=> navigation.pop() }
      />


      <Button 
        title='ir a inicio'
        onPress={ ()=> navigation.popToTop() }
      />
    </View>
  );
}
