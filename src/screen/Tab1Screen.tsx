import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import TouchableIcon from '../components/TouchableIcon';
import { styles } from '../theme/appTheme';

export function Tab1Screen() {
  /* Este efecto se dispara solo caundo se monta el componente, el drawer mantiene su estado una vez visto */
  useEffect(() => {
    console.log('Tabs1Screen effect');
    

  }, [])

  return (
    <View style={ styles.globalMargin }>
      <Text style={ styles.title } > Iconos </Text>
      <Text style={ styles.title } >
        <TouchableIcon iconName='airplane-outline' />
        <TouchableIcon iconName='bonfire-outline' />
        <TouchableIcon iconName='man-outline' />
        <TouchableIcon iconName='woman-outline' />
      </Text>
    </View>
  )
}