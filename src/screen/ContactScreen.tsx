import { View, Text, Button, Alert } from 'react-native'
import React, { useContext, useEffect } from 'react'
import { styles } from '../theme/appTheme'
import { AuthContext } from '../context/AuthContext'
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
  } from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';

export default function ContactScreen() {

  useEffect(() => {
    GoogleSignin.configure({
      scopes: ['email', 'profile'], // what API you want to access on behalf of the user, default is email and profile
      webClientId:
        '1099189131125-4fmij76k1a831ni8sbarrvaebq9ajg5h.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
    });
  }, []);
  const { singIn, authState: { isLoggedIn }, logOut } = useContext( AuthContext )

  const _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const { user: { name,email }, idToken, accessToken} = await GoogleSignin.signIn();
      const credential = auth.GoogleAuthProvider.credential(
        idToken,
        accessToken,
      );
      await auth().signInWithCredential(credential);
      Alert.alert( 'nombre', name || 'No name' )
      Alert.alert( 'email', email )
      singIn();
    } catch (error: any) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        Alert.alert('You canceled the auth process dumpass');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        Alert.alert('Signin in progress');
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        Alert.alert('PLAY_SERVICES_NOT_AVAILABLE');
        // play services not available or outdated
      } else {
        // some other error happened
        Alert.alert(JSON.stringify( error));
        console.log( JSON.stringify( error) );
        
      }
    }
  };

  

  const signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      logOut();
    } catch (error) {
      console.error(error);
    }
  };
  //manera larga const { isLoggedIn } = authState
  return (
    <View style={ styles.globalMargin }   >
      <Text style={ styles.title }>ContactScreen</Text>

      { 
        isLoggedIn ? <Button title='logOut' onPress={ logOut } /> : 
        ( 
          <GoogleSigninButton   
            style={{width: 192, height: 48}}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark} onPress={ _signIn }  
          />
        )
      }
    </View>
  )
}