import { View, Text, Button, TouchableOpacity } from 'react-native';
import React, { useEffect } from 'react';
//import { StackScreenProps } from '@react-navigation/stack';
import { DrawerScreenProps } from '@react-navigation/drawer';
import { colors, styles } from '../theme/appTheme';
import Icon from 'react-native-vector-icons/Ionicons';

interface Props extends DrawerScreenProps <any,any>{}

export default function Page1Screen( { navigation }: Props ) {

  useEffect(() => {
    navigation.setOptions({
      headerLeft: ()=> (
/*         <Button 
          title='menu'
          onPress={ ()=> navigation.toggleDrawer() }
        ></Button> */
        <TouchableOpacity
          activeOpacity={0.3}
          onPress={ ()=> navigation.toggleDrawer() }
          
        >
          <Icon name='menu' color={ colors.primary } size={ 40 } />
        </TouchableOpacity>
      )
    })
  
  }, []);

  return (
    <View style={ styles.globalMargin } >
      <Text style={ styles.title }>Pagina 1 Screen </Text>

      <Button
        title="ir a pagina2"
        onPress={ ()=> navigation.navigate('Page2Screen') }
      />

      <Text style={{ marginVertical: 10, fontSize: 18, textAlign: 'center' }}>Navegar con argumentos</Text>

      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity
          style={{
            ...styles.botonGrande,
            backgroundColor: '#5856D6'
          }}
          onPress={ ()=> navigation.navigate('PersonScreen', {
            id: 1,
            nombre: 'Pedro'
          }) }
        >
          <Text style={{ ...styles.botonGrandeTexto, marginLeft: 5 }} >Pedro  </Text>
          <Text>
            < Icon name='man-outline' size={ 50 } color='lightgray'/>
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            ...styles.botonGrande,
            backgroundColor: '#FF9427'
          }}
          onPress={ ()=> navigation.navigate('PersonScreen', {
            id: 2,
            nombre: 'Maria'
          }) }
        >
          <Text style={ styles.botonGrandeTexto } >Maria</Text>
          <Text>
            < Icon name='woman-outline' size={ 50 } color='lightgray'/>
          </Text>
        </TouchableOpacity>
      </View>
      
    </View>
  );
}
