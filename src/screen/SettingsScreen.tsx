import { View, Text } from 'react-native';
import React, { useContext } from 'react';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import { styles } from '../theme/appTheme';
import { AuthContext } from '../context/AuthContext';

export const SettingsScreen = () => {

  const { authState } = useContext( AuthContext )
  const inset = useSafeAreaInsets()
  return (
    /* Para no usar el safeArea, en lugar poder manejarlo de manera absoluta */
    <View style={{ 
      ...styles.globalMargin,
      marginTop: inset.top + 20}}
    >
      <Text style={ styles.title } >SettingsScreen</Text>
      <Text>
        { JSON.stringify( authState, null,4 ) }
      </Text>
    </View>
  );
}
