
import React, { useReducer } from 'react'
//Definir estructura de datos

import { createContext } from "react"
import { authReducer } from './authReducer'

export interface AuthState {
    isLoggedIn: boolean,
    userName?: string,
    favoriteIcon?: string
}

//estado inicial
export const authInitialState : AuthState = {
    isLoggedIn: false,
    userName: undefined,
    favoriteIcon: undefined
}

// lo usaremos para decirle a reat como luce y que expone el context
export interface AuthContextProps {
    authState: AuthState;
    singIn: () => void,
    logOut: () => void,
    changeFavoriteIcon: (iconName: string) => void
}

//crear el contexto

export const AuthContext = createContext( {} as AuthContextProps )

// Componente que es el provedor de el estado

export const Authprovider = ( { children }: any  ) => {

    const [authState, dispatch] = useReducer( authReducer, authInitialState )

    const singIn = () => {
        dispatch({ type: 'singIn' })
    }

    const changeFavoriteIcon = ( iconName:string ) => {
        dispatch( { type: 'changeFavIcon', payload: iconName } )
    }

    const logOut = () => {
        dispatch({ type: 'logout' })
    }
    return (
        <AuthContext.Provider 
            value={{
                authState,
                singIn,
                changeFavoriteIcon,
                logOut
            }}
        >
            { children }
        </AuthContext.Provider>
    )
}
