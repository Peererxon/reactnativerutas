import { AuthState } from "./AuthContext";

type AuthAction = 
    | { type: 'singIn'  } 
    | { type: 'changeFavIcon', payload: any }
    | { type: 'logout' }

export const authReducer = ( state: AuthState, action: AuthAction ): AuthState => {
    switch ( action.type ) {
        case 'singIn':
            return {
                ...state,
                isLoggedIn: true,
                userName: 'no-username-yet'
            }
        
        case 'changeFavIcon':
            return {
                ...state,
                favoriteIcon: action.payload
            }
        
        case 'logout':
            return {
                ...state,
                isLoggedIn: false,
                userName: undefined,
                favoriteIcon: undefined
            }
        default:
            return state;
    }

}